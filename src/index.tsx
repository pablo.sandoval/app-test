import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { UserProvider, PolicyProvider, PermissionProvider, AuthProvider } from './lib'
import TestHooksProvider from './hooks/TestHooksProvider';
import { CssBaseline } from '@mui/material'
const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);

root.render(
  <TestHooksProvider>
    <UserProvider environment={import.meta.env.VITE_APP}>
      <PolicyProvider environment={import.meta.env.VITE_APP}>
        <PermissionProvider environment={import.meta.env.VITE_APP}>
          <AuthProvider disableAuth={false} environment={import.meta.env.VITE_APP}>
            <CssBaseline></CssBaseline>
            <App />
          </AuthProvider>
        </PermissionProvider>
      </PolicyProvider>
    </UserProvider>
  </TestHooksProvider>
);



// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();