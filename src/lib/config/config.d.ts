export declare const AzureADConfig: (environment?: "dev" | "prod" | "local") => {
    appId: string;
    redirectUri: string;
    scopes: string[];
    authority: string;
};
export declare const ConfigDev: {
    azureAdAuthority: string;
    azureAppId: string;
    azureAdRedirect: string;
    azureAdScope: string;
    backofficeApiUrl: string;
};
export declare const ConfigProd: {
    azureAdAuthority: string;
    azureAppId: string;
    azureAdRedirect: string;
    azureAdScope: string;
    backofficeApiUrl: string;
};
