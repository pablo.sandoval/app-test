export declare const AuthInitialState: {
    user: any;
    isAuthenticated: boolean;
    accessToken: string;
    isLoadingSession: boolean;
    isErrorSession: boolean;
    instance: any;
    isErrorUser: boolean;
    isLoadingUser: boolean;
};
