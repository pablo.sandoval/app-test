import { IPublicClientApplication } from "@azure/msal-browser";
import { UserInterface } from "../models/UserInterface";
export interface AuthStateInterface {
    /**
     * Objeto que contiene la información del usuario.
     */
    user: UserInterface | null;
    /**
     * Indica si el usuario está autenticado.
     */
    isAuthenticated: boolean;
    /**
     * Contiene el token de acceso de la Suite Best4 al iniciar sesión.
     */
    accessToken: string;
    /**
     * Indica si se está cargando la sesión.
     */
    isLoadingSession: boolean;
    /**
     * Indica si hubo algún error al realizar un log in.
     */
    isErrorSession: boolean;
    /**
     * Objeto inicialización utilizado para realizar los procesos de autenticación.
     */
    instance: IPublicClientApplication;
    /**
     * Indica si hubo algún error al realizar la petición de obtención de datos del usuario.
     */
    isErrorUser: boolean;
    /**
     * Indica que se está cargando los datos del usuario.
     */
    isLoadingUser: boolean;
}
