import { PublicClientApplication } from "@azure/msal-browser";
export interface UpdateInstance {
    instance: PublicClientApplication;
}
