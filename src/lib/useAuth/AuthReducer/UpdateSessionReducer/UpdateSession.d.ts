import { UserInterface } from "../../models/UserInterface";
export interface UpdateSession {
    accessToken: string;
    user: UserInterface | null;
    isAuthenticated: boolean;
    isLoadingSession: boolean;
    isErrorSession: boolean;
}
