import { AuthStateInterface } from "../AuthState/AuthInitialStateInterface";
import { authReducerOptions } from "./AuthReducerOptions";
declare const AuthReducer: (state: AuthStateInterface, action: authReducerOptions) => AuthStateInterface | {
    instance: import("@azure/msal-browser").PublicClientApplication;
    user: import("../models/UserInterface").UserInterface;
    isAuthenticated: boolean;
    accessToken: string;
    isLoadingSession: boolean;
    isErrorSession: boolean;
    isErrorUser: boolean;
    isLoadingUser: boolean;
};
export { AuthReducer };
