export declare enum authReducerActions {
    UPDATE_SESSION = 0,
    UPDATE_USER = 1,
    LOADING_SESSION = 2,
    UPDATE_TOKEN = 3,
    UPDATE_INSTANCE = 4
}
