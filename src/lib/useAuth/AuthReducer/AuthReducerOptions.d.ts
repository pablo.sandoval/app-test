import { authReducerActions } from "./AuthReducerActions";
import { LoadingSession } from "./LoadingSessionReducer/LoadingSession";
import { UpdateInstance } from "./UpdateInstanceReducer/UpdateInstanceReducer";
import { UpdateSession } from "./UpdateSessionReducer/UpdateSession";
import { UpdateToken } from "./UpdateTokenReducer/UpdateToken";
import { UpdateUser } from "./UpdateUserReducer/UpdateUser";
export interface authReducerOptions {
    type: authReducerActions;
    data?: UpdateSession | UpdateUser | LoadingSession | UpdateToken | UpdateInstance;
}
