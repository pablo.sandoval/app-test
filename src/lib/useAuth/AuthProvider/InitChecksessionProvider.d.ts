import React from 'react';
interface InitCheckSessionProviderInterface {
    environment: "dev" | "prod" | "local";
    children: JSX.Element | JSX.Element[];
    dummyPermissions: Array<string>;
    disableAuth: boolean;
}
declare const InitCheckSessionProvider: ({ children, environment, disableAuth, dummyPermissions }: InitCheckSessionProviderInterface) => React.JSX.Element;
export default InitCheckSessionProvider;
