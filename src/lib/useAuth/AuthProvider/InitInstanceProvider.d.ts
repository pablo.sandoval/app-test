import { IPublicClientApplication } from '@azure/msal-browser';
import React from 'react';
interface InitInstanceProviderInterface {
    environment: "dev" | "prod" | "local";
    instance: IPublicClientApplication;
    children: JSX.Element | JSX.Element[];
}
declare const InitInstanceProvider: ({ environment, instance, children }: InitInstanceProviderInterface) => React.JSX.Element;
export default InitInstanceProvider;
