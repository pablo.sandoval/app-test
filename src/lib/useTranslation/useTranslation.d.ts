import { TranslationContextInterface } from "./TranslationContext";
export declare const useTranslation: () => TranslationContextInterface;
