import { TranslationContextInterface } from "../TranslationContext";
declare const useTranslationManager: () => TranslationContextInterface;
export { useTranslationManager };
