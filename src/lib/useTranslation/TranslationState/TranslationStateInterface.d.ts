import { LenguageInterface } from "../models/LenguageInterface";
export interface TranslationStateInterface {
    selectedLenguage: LenguageInterface;
    isLoadingLenguage: boolean;
    isErrorLenguage: boolean;
}
