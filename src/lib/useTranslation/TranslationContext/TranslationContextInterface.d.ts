import { TranslationStateInterface } from "../TranslationState/TranslationStateInterface";
import { LenguageInterface } from "../models/LenguageInterface";
export interface TranslationContextInterface extends TranslationStateInterface {
    /**
     *
     * @param scope Planta de la aplicación
     * @param lenguage Lenguaje que se quiere seleccionar
     * @returns
     */
    initLenguage: (i18n: string, lenguage: LenguageInterface) => Promise<boolean>;
    /**
     *
     * @param lenguage Nuevo Lenguaje que se selecciona
     * @returns
     */
    changeLenguage: (lenguage: LenguageInterface) => Promise<boolean>;
    t: (key: string) => string;
}
