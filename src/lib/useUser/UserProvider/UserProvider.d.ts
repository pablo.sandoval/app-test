import React from "react";
interface UserProviderInterface {
    children: JSX.Element | JSX.Element[];
    environment: "dev" | "prod" | "local";
}
declare const _default: React.MemoExoticComponent<({ children, environment }: UserProviderInterface) => React.JSX.Element>;
export default _default;
