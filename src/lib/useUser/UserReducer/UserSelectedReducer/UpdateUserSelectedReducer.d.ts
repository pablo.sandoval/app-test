import { UserListInterface } from "../../core/models/UserInterface";
export interface UpdateUserSelectedReducer {
    userSelected: UserListInterface | undefined;
    isErrorUserSelected: boolean;
    isLoadingUserSelected: boolean;
}
