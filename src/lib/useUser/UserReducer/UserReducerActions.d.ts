export declare enum UserReducerActions {
    UPDATE_USER_LIST = 0,
    LOADING_USER_LIST = 1,
    UPDATE_USER_SELECTED = 2,
    LOADING_USER_SELECTED = 3
}
