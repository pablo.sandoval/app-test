import { Details } from "./Details";
import { ApplicationInterface } from "./ApplicationInterface";
export interface UserListInterface {
    id: string;
    short_email: string;
    large_email?: string;
    sap_id?: string;
    fullName?: string;
    lastLogin?: Date;
    policies?: Details[];
    applications?: ApplicationInterface[];
}
