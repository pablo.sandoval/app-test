export interface ApplicationInterface {
    id: number;
    name: string;
    description: string;
    url: string;
}
