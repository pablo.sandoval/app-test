import { ApplicationInterface } from "./ApplicationInterface";
import { TenantInterface } from "./TenantInterface";
export interface Details {
    role: {
        id: number;
        name: string;
        description: string;
        status: boolean;
        reference: number;
        permissions: {
            id: number;
            name: string;
            description: string;
            status: boolean;
            RolePermissions: {
                id: number;
                roles_id: number;
                permissions_id: number;
            };
        }[];
    };
    tenant: TenantInterface;
    policy: {
        id: number;
        tenant_id: number;
        role_id: number;
        application_id: number;
        user_id: number;
        status: boolean;
        tenant: TenantInterface;
        application: ApplicationInterface;
    };
    permissions: string[];
}
