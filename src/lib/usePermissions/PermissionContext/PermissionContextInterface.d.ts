import { UserInterface } from "../../useAuth/models/UserInterface";
import { PermissionStateInterface } from "../PermissionState/PermissionStateInterface";
import { PermissionInterface } from "../core/models/PermissionInterface";
export interface PermissionContextInterface extends PermissionStateInterface {
    /**
     * Función que trae los permisos específicos de un usuario.
     * @param user Objeto del usuario
     * @param tenant Planta
     * @param type Tipo de la planta
     * @param app Aplicación
     * @returns Verdadero o falso
     */
    getPermissions: (user: UserInterface, tenant: "Santa Fe" | "Guaiba" | "Maule" | "Pacifico" | "Laja" | "some tenant", type: "celulosa" | "biopackaging" | "some tenant type", app: string) => boolean;
    /**
     * Función que trae los permisos de un Rol
     * @param accessToken Token de acceso
     * @param idRole ID del rol
     * @returns Arreglo de permisos
     */
    fetchPermissionsByRole: (accessToken: string, idRole: number) => Promise<PermissionInterface[]>;
    /**
     * Función que actualiza los permisos de un rol.
     * @param accessToken Token de acceso
     * @param idRole ID del rol
     * @param permissions Arreglo de permisos del rol
     * @returns Arreglo de permisos
     */
    updatePermissionsByRole: (accessToken: string, idRole: number, permissions: string[]) => Promise<string[]>;
    /**
     * Función trae todos los permisos de la base de datos.
     * @param accessToken Token de acceso
     * @returns Arreglo de permisos
     */
    fetchPermissions: (accessToken: string) => Promise<PermissionInterface[]>;
    /**
     * Función que actualiza los datos de un permiso en la base de datos.
     * @param accessToken Token de acceso
     * @param idPermission ID del permiso
     * @param permission Objeto del permiso
     * @returns Objeto del permiso con su nueva información
     */
    updatePermission: (accessToken: string, idPermission: number, permission: PermissionInterface) => Promise<PermissionInterface>;
    /**
     * Función que crea un nuevo permiso
     * @param accessToken Token de acceso
     * @param permission Objeto del permiso
     * @returns Objeto del nuevo permiso
     */
    createPermission: (accessToken: string, permission: PermissionInterface) => Promise<PermissionInterface>;
}
