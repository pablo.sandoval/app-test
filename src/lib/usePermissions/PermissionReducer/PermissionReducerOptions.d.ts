import { LoadingPermissionListReducer } from "./PermissionListReducer/LoadingPermissionListReducer";
import { UpdatePermissionListReducer } from "./PermissionListReducer/UpdatePermissionListReducer";
import { LoadingPermissionReducer } from "./PermissionReducer/LoadingPermissionReducer";
import { UpdatePermissionReducer } from "./PermissionReducer/UpdatePermissionReducer";
import { PermissionReducerActions } from "./PermissionReducerActions";
export interface PermissionReducerOptions {
    type: PermissionReducerActions;
    data?: UpdatePermissionListReducer | UpdatePermissionReducer | LoadingPermissionListReducer | LoadingPermissionReducer;
}
