export interface UpdatePermissionReducer {
    permissionsByRole: string[];
    isLoadingPermissions: boolean;
    isErrorPermissions: boolean;
}
