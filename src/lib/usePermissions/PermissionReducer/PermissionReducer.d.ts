import { PermissionStateInterface } from "../PermissionState/PermissionStateInterface";
import { PermissionReducerOptions } from "./PermissionReducerOptions";
declare const PermissionReducer: (state: PermissionStateInterface, action: PermissionReducerOptions) => {
    isLoadingPermissions: boolean;
    permissionsList: (string | import("../..").PermissionInterface)[];
    permissionsByRole: string[];
    isLoadingPermissionsList: boolean;
    isErrorPermissionsList: boolean;
    isErrorPermissions: boolean;
};
export { PermissionReducer };
