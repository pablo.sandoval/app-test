export interface UpdatePermissionListReducer {
    permissionsList: Array<string>;
    isLoadingPermissionsList: boolean;
    isErrorPermissionsList: boolean;
}
