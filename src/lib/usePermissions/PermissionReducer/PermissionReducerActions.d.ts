export declare enum PermissionReducerActions {
    UPDATE_PERMISSIONS_LIST = 0,
    LOADING_PERMISSIONS_LIST = 1,
    UPDATE_PERMISSIONS = 2,
    LOADING_PERMISSIONS = 3
}
