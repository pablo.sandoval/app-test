import React from "react";
interface PermissionProviderInterface {
    children: JSX.Element | JSX.Element[];
    environment?: "dev" | "prod" | "local";
    disabled?: boolean;
}
declare const _default: React.MemoExoticComponent<({ children, environment }: PermissionProviderInterface) => React.JSX.Element>;
export default _default;
