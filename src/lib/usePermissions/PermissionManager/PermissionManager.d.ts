import { PermissionContextInterface } from "../PermissionContext/PermissionContextInterface";
declare const usePermissionManager: (environment: "local" | "prod" | "dev") => PermissionContextInterface;
export { usePermissionManager };
