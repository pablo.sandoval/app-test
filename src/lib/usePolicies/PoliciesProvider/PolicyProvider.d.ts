import React from 'react';
interface PolicyProviderInterface {
    children: JSX.Element | JSX.Element[];
    environment: "dev" | "prod" | "local";
}
declare const _default: React.MemoExoticComponent<({ children, environment }: PolicyProviderInterface) => React.JSX.Element>;
export default _default;
