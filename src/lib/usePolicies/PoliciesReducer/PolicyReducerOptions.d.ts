import { PolicyReducerActions } from "./PolicyReducerActions";
import { LoadingPolicyReducer } from "./PolicyReducers/LoadingPolicy";
import { UpdatePolicyReducer } from "./PolicyReducers/UpdatePolicy";
export interface PolicyReducerOptions {
    type: PolicyReducerActions;
    data?: UpdatePolicyReducer | LoadingPolicyReducer;
}
