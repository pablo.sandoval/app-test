import { PolicyStateInterface } from "../PoliciesState/PolicyStateInterface";
import { PolicyInterface } from "../core/models/PolicyInterface";
import { UserPolicyInterface } from "../core/models/UserPolicyInterface";
export interface PolicyContextInterface extends PolicyStateInterface {
    /**
     * Trae todas las políticas de Best4.
     * @param accessToken Token de acceso.
     * @returns Arreglo de políticas.
     */
    fetchPolicies: (accessToken: string) => Promise<PolicyInterface[]>;
    /**
     * Permite crear una nueva política a un usuario.
     * @param accessToken Token de acceso.
     * @param policy Objeto con los datos necesarios.
     * @returns Nueva política creada.
     */
    createPolicy: (accessToken: string, policy: UserPolicyInterface) => Promise<PolicyInterface>;
    /**
     * Permite eliminar una política en específico.
     * @param accessToken Token de acceso.
     * @param id Id de la política.
     * @returns Verdadero o Falso.
     */
    deletePolicy: (accessToken: string, idPolicy: number) => Promise<boolean>;
    /**
     * Permite actualizar los datos de una política.
     * @param accessToken Token de acceso.
     * @param policy Objeto con los nuevos datos necesarios.
     * @returns Nueva política creada.
     */
    updatePolicy: (accessToken: string, policy: PolicyInterface) => Promise<boolean>;
}
