export interface PolicyInterface {
    id: number;
    tenant_id: number;
    role_id: number;
    application_id: number;
    user_id: number;
    status: boolean;
}
