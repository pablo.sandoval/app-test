import { PolicyContextInterface } from '../PoliciesContext/PolicyContextInterface';
declare const usePoliciesManager: (environment: "prod" | "dev" | "local") => PolicyContextInterface;
export default usePoliciesManager;
