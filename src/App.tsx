import React,{ useEffect, useState } from 'react';
import './App.css';
import { useAuth } from './lib';
import { Applications, Authentication, CreatePolicy, Policy, Users, Roles } from './components'
import {ToggleButton,ToggleButtonGroup} from "@mui/material"
import CreateUsers from './components/CreateUsers';
import CreatePolicies from './components/CreatePolicies';
import TestComponent from './components/TestComponent';
import FormProvider from './hooks/Test/FormProvider';



function App() {
  const { isAuthenticated,logIn,logOut,user,checkSession, instance } = useAuth()
  const [nav,setNav] = useState<"users" | "policy" | "auth" | "apps" | "createPolicy" | "roles" | "createUsers" | "createPolicies" | "test">("auth")
  const [toggle,setToggle] = useState(0)
  console.debug("Autenticado: ",isAuthenticated)
  useEffect(() => {
    fetch('./components/CreatePolicies').then(async (value) => {
        
    })
  },[])

  useEffect(() => {
    //setTimeout(() => checkSession('local'), 100)
  },[checkSession])

  const handleAlignment = (
    event: React.MouseEvent<HTMLElement>,
    newAlignment: number,
  ) => {
    setToggle(newAlignment);
  };
  const test = [
    {
        "id": 3,
        "name": "carta_farol.sca.create",
        "description": "Crear",
        "status": true
    },
    {
        "id": 2,
        "name": "carta_farol.sca.edit",
        "description": "Editar",
        "status": true
    },
    {
        "id": 4,
        "name": "carta_farol.sca.delete",
        "description": "Eliminar",
        "status": true
    },
    {
        "id": 1,
        "name": "control_kappa.sca.read",
        "description": "Ver",
        "status": true
    },
    {
        "id": 6,
        "name": "dynamic_recommender.recommender.read",
        "description": "Lectura",
        "status": true
    },
    {
        "id": 7,
        "name": "dynamic_recommender.recommender.edit",
        "description": "Editar",
        "status": true
    },
    {
        "id": 8,
        "name": "dynamic_recommender.adherence.read",
        "description": "Lectura",
        "status": true
    },
    {
        "id": 9,
        "name": "dynamic_recommender.binnacle.read",
        "description": "Lectura",
        "status": true
    },
    {
        "id": 11,
        "name": "carta_farol.sca.read",
        "description": "Ver",
        "status": true
    },
    {
        "id": 10,
        "name": "dynamic_recommender.binnacle.edit",
        "description": "Editar",
        "status": true
    },
    {
        "id": 12,
        "name": "recovery_cauldron.monitoring.read",
        "description": "Ver",
        "status": true
    },
    {
        "id": 13,
        "name": "recovery_cauldron.binnacle.read",
        "description": "Ver",
        "status": true
    },
    {
        "id": 105,
        "name": "control_kappa.l2.monitoring.read",
        "description": "Permiso para ver monitoreo en Línea 2.",
        "status": true
    },
    {
        "id": 111,
        "name": "dynamic_recommender.binnacle.edit",
        "description": "Permiso para editar bitácoras.",
        "status": true
    },
    {
        "id": 116,
        "name": "carta_farol.maintainer.read",
        "description": "Permiso para ver el mantenedor",
        "status": true
    },
    {
        "id": 121,
        "name": "carta_farol.binnacle.write",
        "description": "Permiso para escribir bitácoras",
        "status": true
    },
    {
        "id": 102,
        "name": "control_kappa.l1.monitoring.read",
        "description": "Permiso para ver monitoreo en Línea 1.",
        "status": true
    },
    {
        "id": 106,
        "name": "recovery_cauldron.monitoring.read",
        "description": "Permiso para ver monitoreo.",
        "status": true
    },
    {
        "id": 107,
        "name": "dynamic_recommender.recommender.read",
        "description": "Permiso para ver recomendador.",
        "status": true
    },
    {
        "id": 109,
        "name": "dynamic_recommender.binnacle.read",
        "description": "Permiso para ver bitácoras.",
        "status": true
    },
    {
        "id": 112,
        "name": "dynamic_recommender.binnacle.create",
        "description": "Permiso para crear una bitácora.",
        "status": true
    },
    {
        "id": 115,
        "name": "carta_farol.sca.write",
        "description": "Permiso para escribir y modificar SCA",
        "status": true
    },
    {
        "id": 117,
        "name": "carta_farol.maintainer.write",
        "description": "Permiso para actualziar variables del mantenedor",
        "status": true
    },
    {
        "id": 118,
        "name": "carta_farol.maintainer.delete",
        "description": "Permiso para eliminar variables del mantenedor",
        "status": true
    },
    {
        "id": 123,
        "name": "carta_farol.config",
        "description": "Permiso que permite configurar la aplicación de la Carta Farol",
        "status": true
    },
    {
        "id": 173,
        "name": "app_ady.monitoring.edit",
        "description": "Permite editar las pantallas",
        "status": true
    },
    {
        "id": 174,
        "name": "app_ady.monitoring.read",
        "description": "Permite ver las pantallas",
        "status": true
    },
    {
        "id": 103,
        "name": "recovery_cauldron.binnacle.read",
        "description": "Permiso para ver las bitácoras.",
        "status": true
    },
    {
        "id": 108,
        "name": "dynamic_recommender.recommender.edit",
        "description": "Permiso para editar en recomendador.",
        "status": true
    },
    {
        "id": 114,
        "name": "carta_farol.sca.read",
        "description": "Permiso para ver SCA",
        "status": true
    },
    {
        "id": 119,
        "name": "carta_farol.binnacle.read",
        "description": "Permiso para leer las bitácoras",
        "status": true
    },
    {
        "id": 122,
        "name": "carta_farol.binnacle.delete",
        "description": "Permiso para eliminar bitácoras",
        "status": true
    },
    {
        "id": 124,
        "name": "laja.mantainer.config",
        "description": "Permiso para leer laja",
        "status": true
    },
    {
        "id": 125,
        "name": "cordillera.mantainer.config",
        "description": "Permiso para leer cordillera",
        "status": null
    },
    {
        "id": 104,
        "name": "recovery_cauldron.binnacle.create",
        "description": "Permiso para crear bitácoras.",
        "status": true
    },
    {
        "id": 110,
        "name": "dynamic_recommender.adherence.read",
        "description": "Permiso para ver adherencia.",
        "status": true
    },
    {
        "id": 113,
        "name": "carta_farol.monitoring.read",
        "description": "Permiso para acceder a la pantalla de monitoreo",
        "status": true
    },
    {
        "id": 120,
        "name": "carta_farol.maintainer.config",
        "description": "Permiso para configurar los productos de la Carta Farol",
        "status": true
    }
].map(policy => {
  return policy.id
})

  const NavRender = () => {
    switch (nav) {
        case 'auth':
            return <Authentication></Authentication>
        case 'policy':
            return <Policy></Policy>
        case 'users':
            return <Users></Users>
        case 'apps':
            return <Applications></Applications>
        case 'createPolicy':
            return <CreatePolicy></CreatePolicy>
        case 'roles':
            return <Roles></Roles>
        case 'createUsers':
            return <CreateUsers></CreateUsers>
        case 'createPolicies':
            return <CreatePolicies></CreatePolicies>
        case 'test':
            return <FormProvider><TestComponent></TestComponent></FormProvider>
        default:
            break;
    }
  }
  return (
    <div className="App">
        <div className='Nav'>
            <ToggleButtonGroup value={toggle} onChange={handleAlignment} exclusive>
              <ToggleButton value={0} onClick={() => setNav("auth")}>Autorización</ToggleButton>
              <ToggleButton value={8} onClick={() => setNav("test")}>Pruebas</ToggleButton>
              {isAuthenticated && <ToggleButton value={1} onClick={() => setNav("users")}>Usuarios</ToggleButton>}
              {isAuthenticated && <ToggleButton value={2} onClick={() => setNav("policy")}>Políticas</ToggleButton>}
              {isAuthenticated && <ToggleButton value={3} onClick={() => setNav("apps")}>Aplicaciones</ToggleButton>}
              {isAuthenticated && <ToggleButton value={4} onClick={() => setNav("roles")}>Roles y permisos</ToggleButton>}
              {isAuthenticated && <ToggleButton value={5} onClick={() => setNav("createPolicy")}>Crear Política</ToggleButton>}
              {isAuthenticated && <ToggleButton value={6} onClick={() => setNav("createPolicies")}>Crear Políticas</ToggleButton>}
              {isAuthenticated && <ToggleButton value={7} onClick={() => setNav("createUsers")}>Crear Usuarios</ToggleButton>}
            </ToggleButtonGroup>
            
            
        </div>
      {NavRender()}
    </div>
  );
}

export default React.memo(App);
