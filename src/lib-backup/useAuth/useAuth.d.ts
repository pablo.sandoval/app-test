import { AuthContextInterface } from "./AuthContext/AuthContextInterface";
export declare const useAuth: () => AuthContextInterface;
