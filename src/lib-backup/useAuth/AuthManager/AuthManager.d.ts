import { AuthContextInterface } from "../AuthContext/AuthContextInterface";
declare const useAuthManager: () => AuthContextInterface;
export default useAuthManager;
