export interface UserInterface {
    id: number;
    email: string;
    fullName?: string;
    hasBackoffice?: boolean;
    tenants?: {
        name: string;
        description?: string;
        type: string;
        applications: {
            id?: string;
            name: string;
            description: string;
            url: string;
            permissions: string[];
        }[];
    }[];
}
