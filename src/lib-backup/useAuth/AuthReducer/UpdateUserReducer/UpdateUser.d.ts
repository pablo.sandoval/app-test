import { UserInterface } from "../../models/UserInterface";
export interface UpdateUser {
    user: UserInterface | null;
    isErrorUser: boolean;
    isLoadingUser: boolean;
}
