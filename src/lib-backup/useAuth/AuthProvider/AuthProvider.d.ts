import React from "react";
interface AuthProviderInterface {
    children: JSX.Element | JSX.Element[];
    environment?: "dev" | "prod" | "local";
    dummyPermissions?: Array<string>;
    disableAuth?: boolean;
}
declare const _default: React.MemoExoticComponent<({ children, dummyPermissions, environment, disableAuth }: AuthProviderInterface) => React.JSX.Element>;
export default _default;
