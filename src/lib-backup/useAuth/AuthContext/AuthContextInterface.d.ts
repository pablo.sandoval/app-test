import { AuthStateInterface } from "../AuthState/AuthInitialStateInterface";
import { UserInterface } from "../models/UserInterface";
export interface AuthContextInterface extends AuthStateInterface {
    /**
     * Función que permite verificar si existe una sesión activa.
     * @param environment Entorno de desarrollo (Opcional)
     * @param disabled Desactivar autenticación para traer usuario de prueba (Opcional)
     * @param dummyPermissions Permisos de pruebas para el usuario de prueba (Opcional)
     * @returns Verdadero o Falso
     */
    checkSession: (environment?: "dev" | "prod" | "local", disabled?: boolean, dummyPermissions?: Array<string>) => Promise<boolean>;
    /**
     * Permite iniciar sesión en la plataforma de la Suite Best4.
     * @param environment Entorno de desarrollo (Opcional)
     * @returns Datos del Usuario o null
     */
    logIn: (environment?: "dev" | "prod" | "local") => Promise<UserInterface | null>;
    /**
     * Permite cerrar sesión en la plataforma de la Suite Best4.
     * @returns Verdadero o Falso
     */
    logOut: () => Promise<boolean>;
    /**
     * Permite actualizar la información del usuario (Deprecado)
     * @returns null
     */
    updateUser: () => Promise<UserInterface | null>;
    /**
     * Ayuda a generar una instancia de Azure para iniciar sesión.
     * @param environment Entorno de desarrollo
     * @returns void
     */
    initInstance: (environment: "prod" | "dev" | "local") => void;
}
