import { PublicClientApplication } from "@azure/msal-browser";
export declare const getClient: () => PublicClientApplication;
export declare const AuthInitialState: {
    user: any;
    isAuthenticated: boolean;
    accessToken: string;
    isLoadingSession: boolean;
    isErrorSession: boolean;
    instance: PublicClientApplication;
    isErrorUser: boolean;
    isLoadingUser: boolean;
};
