import React from "react";
import "../i18n/i18n";
interface TranslationProviderInterface {
    children: JSX.Element | JSX.Element[];
}
declare const _default: React.MemoExoticComponent<({ children }: TranslationProviderInterface) => React.JSX.Element>;
export default _default;
