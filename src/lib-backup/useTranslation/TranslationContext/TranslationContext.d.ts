/// <reference types="react" />
import { TranslationContextInterface } from "./TranslationContextInterface";
export declare const TranslationContext: import("react").Context<TranslationContextInterface>;
