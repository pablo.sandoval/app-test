import { PolicyContextInterface } from "./PoliciesContext/PolicyContextInterface";
export declare const usePolicies: () => PolicyContextInterface;
