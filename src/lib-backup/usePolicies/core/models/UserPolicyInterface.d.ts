export interface UserPolicyInterface {
    user_id: number;
    tenant_id: number;
    role_id: number;
    application_id: number;
    permissions: number[];
}
