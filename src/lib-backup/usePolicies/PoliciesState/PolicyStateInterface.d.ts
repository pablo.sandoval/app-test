import { PolicyInterface } from "../core/models/PolicyInterface";
export interface PolicyStateInterface {
    /**
     * Contiene un arreglo con todas las políticas obtenidas de fetchPolicies.
     */
    policyList: PolicyInterface[];
    /**
     * Indica si están cargando las políticas.
     */
    isLoadingPolicies: boolean;
    /**
     * Indica si hubo un error al cargar las políticas.
     */
    isErrorPolicies: boolean;
}
