import { PolicyInterface } from "../../core/models/PolicyInterface";
export interface UpdatePolicyReducer {
    policyList: PolicyInterface[];
    isLoadingPolicies: boolean;
    isErrorPolicies: boolean;
}
