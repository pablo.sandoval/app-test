import { PolicyStateInterface } from "../PoliciesState/PolicyStateInterface";
import { PolicyReducerOptions } from "./PolicyReducerOptions";
declare const PolicyReducer: (state: PolicyStateInterface, action: PolicyReducerOptions) => {
    isLoadingPolicies: boolean;
    isErrorPolicies: boolean;
    policyList: import("../core/models/PolicyInterface").PolicyInterface[];
};
export { PolicyReducer };
