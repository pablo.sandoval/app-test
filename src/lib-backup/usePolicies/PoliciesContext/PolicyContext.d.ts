/// <reference types="react" />
import { PolicyContextInterface } from "./PolicyContextInterface";
export declare const PolicyContext: import("react").Context<PolicyContextInterface>;
