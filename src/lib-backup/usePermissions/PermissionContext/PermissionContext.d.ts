/// <reference types="react" />
import { PermissionContextInterface } from "./PermissionContextInterface";
export declare const PermissionContext: import("react").Context<PermissionContextInterface>;
