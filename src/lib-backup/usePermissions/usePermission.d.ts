import { PermissionContextInterface } from "./PermissionContext/PermissionContextInterface";
export declare const usePermission: () => PermissionContextInterface;
