import { PermissionInterface } from "../core/models/PermissionInterface";
export interface PermissionStateInterface {
    /**
     * Lista de permisos
     */
    permissionsList: Array<string | PermissionInterface>;
    /**
     * Lista de permisos traidos por fetchPermissionsByRole.
     */
    permissionsByRole: Array<string>;
    /**
     * Indica si se está cargando la lista de permisos.
     */
    isLoadingPermissionsList: boolean;
    /**
     * Indica si hubo un error al cargar la lista de permisos
     */
    isErrorPermissionsList: boolean;
    /**
     * Indica la carga de permiso por rol, creación u actualización.
     */
    isLoadingPermissions: boolean;
    /**
     * Indica si hubo un error en la carga, creación o actualización de permiso.
     */
    isErrorPermissions: boolean;
}
