import { UserContextInterface } from "../UserContext/UserContextInterface";
declare const useUserManager: (environment: "prod" | "dev" | "local") => UserContextInterface;
export { useUserManager };
