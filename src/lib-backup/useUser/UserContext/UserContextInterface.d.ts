import { UserStateInterface } from "../UserState/UserStateInterface";
import { UserListInterface } from "../core/models/UserInterface";
export interface UserContextInterface extends UserStateInterface {
    /**
     * Función que trae toda la lista de usuarios de la base de datos
     * @param accessToken Access Token
     * @param filters Filtros aplicados
     * @returns Lista de usuarios
     */
    fetchUsers: (accessToken: string, filter?: {}) => Promise<UserListInterface[]>;
    /**
     * Función que trae los detalles de un usuario tales como sus políticas, roles y permisos.
     * @param accessToken Access Token
     * @param id Id de usuario
     * @returns Detalles del usuario
     */
    fetchUserDetails: (accessToken: string, id: number) => Promise<UserListInterface | undefined>;
    /**
     * Función que actualiza la carga de los detalles del usuario
     * @param loading Loading
     */
    loadingUserSelected: (loading: boolean) => void;
    /**
     * Función que actualiza la información de usuarios en la base de datos
     * @param accessToken Access Token
     * @param id Id de usuario
     * @returns Información de usuario actualizado
     */
    updateUsers: (accessToken: string, user: UserListInterface) => Promise<UserListInterface | undefined>;
    /**
     * Función que actualiza la lista de usuarios
     * @param userList Nueva lista de usuarios para actualizar el estado
     * @returns Verdadero o falso
     */
    updateUserList: (userList: UserListInterface[]) => boolean;
    /**
     * Función que elimina usuarios en la base de datos
     * @param accessToken Access Token
     * @param id Id de usuario
     * @returns Información del usuario eliminado
     */
    deleteUsers: (accessToken: string, id: number) => Promise<UserListInterface | undefined>;
}
