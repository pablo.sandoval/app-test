export interface TenantInterface {
    id: number;
    name: string;
    type: string;
    status: boolean;
}
