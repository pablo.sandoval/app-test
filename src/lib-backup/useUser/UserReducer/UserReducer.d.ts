import { UserStateInterface } from "../UserState/UserStateInterface";
import { UserReducerOptions } from "./UserReducerOptions";
declare const UserReducer: (state: UserStateInterface, action: UserReducerOptions) => UserStateInterface;
export { UserReducer };
