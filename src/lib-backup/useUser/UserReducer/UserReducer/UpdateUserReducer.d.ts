import { UserListInterface } from "../../core/models/UserInterface";
export interface UpdateUserReducer {
    userList: UserListInterface[];
    isErrorUserList: boolean;
    isLoadingUserList: boolean;
}
