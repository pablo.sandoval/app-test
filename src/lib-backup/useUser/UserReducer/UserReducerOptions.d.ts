import { LoadingUserReducer } from "./UserReducer/LoadingUserReducer";
import { UpdateUserReducer } from "./UserReducer/UpdateUserReducer";
import { UserReducerActions } from "./UserReducerActions";
import { LoadingUserSelectedReducer } from "./UserSelectedReducer/LoadingUserSelectedReducer";
import { UpdateUserSelectedReducer } from "./UserSelectedReducer/UpdateUserSelectedReducer";
export interface UserReducerOptions {
    type: UserReducerActions;
    data?: UpdateUserReducer | LoadingUserReducer | UpdateUserSelectedReducer | LoadingUserSelectedReducer;
}
