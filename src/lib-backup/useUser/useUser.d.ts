import { UserContextInterface } from "./UserContext/UserContextInterface";
export declare const useUser: () => UserContextInterface;
