import { UserListInterface } from "../core/models/UserInterface";
export interface UserStateInterface {
    userList: UserListInterface[];
    isLoadingUserList: boolean;
    isErrorUserList: boolean;
    userSelected: UserListInterface;
    isLoadingUserSelected: boolean;
    isErrorUserSelected: boolean;
}
