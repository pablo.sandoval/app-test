import React, { useEffect, useState } from 'react'
import { useAuth, usePolicies, useUser } from '../lib'
import '../App.css'
import { Table, Select, MenuItem, TableHead, TableBody, TableRow, TableCell, Button, Icon } from '@mui/material'
import { Delete } from '@mui/icons-material'

const Policy = () => {
  const { userList, fetchUsers, userSelected, fetchUserDetails, isLoadingUserList, isLoadingUserSelected, loadingUserSelected } = useUser()
  const { deletePolicy } = usePolicies()
  const { accessToken } = useAuth()
  const [idUser, setIdUser] = useState<number>(0)


  const handleDelete = (policy: any) => {
    deletePolicy(accessToken,policy.id).catch(() => {
      fetchUserDetails(accessToken,policy.user_id)
    })
  }

  const handleOnChange = (id: number) => {
    fetchUserDetails(accessToken, id)
  }

  console.debug(userSelected)

  useEffect(() => {
    if (accessToken) {
      userList.length === 0 && fetchUsers(accessToken).then(() => {
        console.debug("Se obtuvieron los usuarios: ", userList)
      })
    }
    return () => loadingUserSelected(true)
  }, [accessToken, userList])
  return (
    <div className='policies'>
      <h1>Políticas</h1>
      <div className='search-policies'>
        <label>Email Usuario</label>
        <Select aria-label='Email Usuario' style={{
          width: 328
        }} onChange={(e) => {
          handleOnChange(Number(e.target.value))
        }}>
          <MenuItem value={0}>{">>"}Elige un usuario{"<<"}</MenuItem>
          {userList.sort((a: any, b: any) => {
            return a.short_email.localeCompare(b.short_email)
          })
            .map((user, index: number) => {
              return <MenuItem value={user.id}>{user.short_email}</MenuItem>
            })}
        </Select>
      </div>
      <br></br>
      {(!isLoadingUserList && !isLoadingUserSelected) ?
        <div>
          <Table className='table' style={{
            width: "100%"
          }}>
            <TableHead>
              <TableRow>
                <TableCell component="th">ID</TableCell>
                <TableCell component="th">ID Rol</TableCell>
                <TableCell component="th">Planta</TableCell>
                <TableCell component="th">Aplicacion</TableCell>
                <TableCell component="th">Permisos</TableCell>
                <TableCell component="th">ID Status</TableCell>
                <TableCell component="th">Accion</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {userSelected && userSelected.policies?.map((policy) => {
                return (
                  <TableRow>
                    <TableCell component={"td"}>{policy && policy.policy && policy.policy.id}</TableCell>
                    <TableCell component={"td"}>{policy && policy.role && policy.role.id}</TableCell>
                    <TableCell component={"td"}>{policy && policy.tenant && policy.tenant.name}</TableCell>
                    <TableCell component={"td"}>{policy && policy.policy && policy.policy.application.description}</TableCell>
                    <TableCell component={"td"} style={{
                      textAlign: "start"
                    }}>{policy && policy.permissions && policy.permissions.map((permission) => {
                      return <span style={{
                        padding: "0px 4px",
                        textAlign: "start",
                        wordBreak: "break-all"
                      }}>{permission}</span>
                    })}</TableCell>
                    <TableCell component={"td"}>{policy && policy.policy && String(policy.policy.status)}</TableCell>
                    <TableCell component={"td"}><Button startIcon={<Delete/>} onClick={() => handleDelete(policy.policy)}>Eliminar</Button></TableCell>
                  </TableRow>
                )
              })}
            </TableBody>
          </Table>
        </div> : <div></div>}


      <p></p>
    </div>
  )
}

export default Policy