import React, {useEffect} from 'react'
import { useAuth } from '../lib'
import { useTestHook } from '../hooks/useTestHook'
import { Table, Button,TableHead,TableBody,TableRow,TableCell } from '@mui/material'
import { Refresh } from '@mui/icons-material'

const Applications = () => {
  const { apps, getApps,permissions, getPermissions } = useTestHook()
  const { accessToken } = useAuth()

  const handleRefresh = () => {
    getApps(accessToken).then(() => {
      console.debug("Aplicaciones Refrescadas.")
    })
  }

  useEffect(() => {
    if (accessToken) {
      apps.length === 0 && getApps(accessToken).then(() => {
        console.debug("Se obtuvieron las Aplicaciones: ", apps)
      })
      permissions.length === 0 && getPermissions(accessToken).then(() => {
        console.debug("Se obtuvieron las Aplicaciones: ", apps)
      })
    }
  }, [apps, accessToken, getApps, getPermissions])

  return (
    <div className='policies'>
      <h1>Aplicaciones</h1>
      <br></br>
      <Button startIcon={<Refresh/>} variant='contained' onClick={handleRefresh}>Refrescar</Button>
      <Table className='table'>
        <TableHead>
        <TableRow>
          <TableCell component={"th"}>ID</TableCell>
          <TableCell component={"th"}>Name</TableCell>
          <TableCell component={"th"}>Description</TableCell>
          <TableCell component={"th"}>URL</TableCell>
          <TableCell component={"th"}>Permisos</TableCell>
        </TableRow>
        </TableHead>
        
        {apps.length > 0 && apps.map((app) => {
          return <TableRow>
            <TableCell>{app.id}</TableCell>
            <TableCell>{app.name}</TableCell>
            <TableCell>{app.description}</TableCell>
            <TableCell>{app.url}</TableCell>
            <TableCell style={{
              display: "flex",
              flexDirection: "column",
              alignContent: "start",
              justifyContent: "start"
            }}>{permissions && 
              permissions.filter((permission) => permission.name.split(".")[0] === app.name)
              .sort((a: any, b: any) => {
                const aName = a.name.split(".")
                const bName = b.name.split(".")
                return aName[aName.length - 2].localeCompare(bName[bName.length - 2])
              })
              .map((permission) => {
              return <span>({permission.id}){permission.name}</span>
            })}</TableCell>
          </TableRow>
        })}
      </Table>
      <p></p>
    </div>
  )
}

export default Applications