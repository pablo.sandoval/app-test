import React, { useContext } from 'react'
import { FormContext } from '../hooks/Test/FormContext'

const TestComponent = () => {
  const { form,setForm } = useContext(FormContext)
  console.debug(form)
  return (
    <div>
      <h5>InputA: {form.inputA}</h5>
      <h5>InputB: {form.inputB}</h5>
      <h5>InputC: {form.inputC}</h5>
      <div>
        <input onChange={(event) => setForm({...form,inputA: event.target.value })}></input>
        <input onChange={(event) => setForm({...form,inputB: event.target.value })}></input>
        <input onChange={(event) => setForm({...form,inputC: event.target.value })}></input>
      </div>
    </div>
  )
}

export default TestComponent