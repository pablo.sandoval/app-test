import React, { Fragment, useEffect,useMemo } from 'react'
import { useAuth, usePermission } from '../lib'
import { PermissionInterface } from '../lib/hooks/usePermissions/core/models/PermissionInterface'

const Roles = () => {
  const { accessToken } = useAuth()
  const {fetchPermissions, permissionsList, isLoadingPermissionsList} = usePermission()
  const permissionsListMemo = useMemo(() => permissionsList as PermissionInterface[],[permissionsList])

  useEffect(() => {
    fetchPermissions(accessToken).then((e) => {
      console.debug("Listo lso permissos",e)
    })
  },[])

  return (
    <Fragment>
      {isLoadingPermissionsList 
      ? <h3>Cargando...</h3>
      : <div>{permissionsListMemo.map((permission:PermissionInterface) => permission.name)}</div>}
    </Fragment>
  )
}

export default Roles