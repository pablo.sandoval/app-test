import { Button } from '@mui/material'
import React, { Fragment, useState } from 'react'
import * as XLSX from 'xlsx'
import { useAuth, usePolicies, useUser } from '../lib'
import { useTestHook } from '../hooks/useTestHook'

interface FormCreatePolicyInterface {
  user_id: number,
  application_id: number,
  tenant_id: number,
  role_id: number,
  permissions: number[]
}

const CreatePolicies = () => {

  const { createUser } = useTestHook()
  const { createPolicy } = usePolicies()
  const { accessToken } = useAuth()

  const [excelFile, setExcelFile] = useState<any>()

  const [excelData, setExcelData] = useState<any>()

  const handleFile = (e: any) => {
    const selectedFile = e.target.files[0];
    if (selectedFile) {
      console.log(selectedFile)
      const readerFile = new FileReader()
      readerFile.readAsArrayBuffer(selectedFile)
      readerFile.onload = (e) => {
        setExcelFile(e.target?.result)
      }
    } else {
      console.log('Selecciona un archivo porfavor.')
    }
    return e
  }
  const handleUpload = (e: any) => {
    if (excelFile) {
      const workbook = XLSX.read(excelFile, { type: 'buffer', })
      const workname = workbook.SheetNames[0]
      const worksheet = workbook.Sheets[workname]
      const data = XLSX.utils.sheet_to_json(worksheet)
      console.debug(data)
      setExcelData(data)
    }
  }
  const handleCreateUsers = () => {
    if (excelData) {
      const promises: Array<any> = []
      excelData.forEach((policy: any, index: number) => {
        const permissions = []
        policy.monitoring_read && permissions.push(113)
        policy.sca_read && permissions.push(114)
        policy.sca_write && permissions.push(115)
        policy.maintainer_read && permissions.push(116)
        policy.maintainer_write && permissions.push(117)
        policy.maintainer_delete && permissions.push(118)
        policy.binnacle_read && permissions.push(119)
        policy.binnacle_write && permissions.push(121)
        policy.binnacle_edit && permissions.push(187)
        if(permissions.length > 0) {
          const newPolicy:FormCreatePolicyInterface = {
            application_id: policy.application_id,
            role_id: 3,
            tenant_id: policy.tenant_id,
            user_id: policy.user_id,
            permissions: permissions
          }
          promises.push(new Promise (() => setTimeout(() => {
            createPolicy(accessToken, newPolicy).then(() => {
              console.log(`Política creada con éxito.`,newPolicy)
            })
            return true
          }, 2000 + 2000 * index)))
        }
        
      });
      Promise.all(promises).then((values) => {
        console.log('Todas las políticas creadas con éxito.',values)
      })
      console.debug('Cargando ...')
      return
    }
    console.debug('No hay datos cargados')
  }

  return (<Fragment>
    <div style={{
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      flexDirection: "column",
    }}>
      <input style={{
        marginBottom: '12px'
      }} type='file' accept='.xlsx' onChange={(e) => {
        console.debug(e)
        handleFile(e)
      }}></input>
      <div className='container-row'>
        <Button variant='outlined' onClick={handleUpload}>Cargar Datos</Button>
        <Button variant='contained' onClick={handleCreateUsers}>Crear Nuevos Usuarios</Button>
      </div>

    </div>

  </Fragment>

  )
}

export default CreatePolicies