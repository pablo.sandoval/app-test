import React, { useEffect, useState } from 'react'
import { useTestHook } from '../hooks/useTestHook'
import { UserListInterface, useAuth, useUser } from '../lib'
import CreateUser from './CreateUser'
import { Table, Button, TableHead, TableBody, TableRow, TableCell, TextField } from '@mui/material'
import { Refresh } from '@mui/icons-material'

const Users = () => {
  const { userList, fetchUsers, fetchUserDetails, userSelected } = useUser()
  const { accessToken } = useAuth()
  const [email, setEmail] = useState<string>()


  const filterUsers = email ? userList.filter((user) => user.short_email === email).sort((a: any, b: any) => {
    return a.short_email.localeCompare(b.short_email)
  }) : userList.sort((a: any, b: any) => {
    return a.short_email.localeCompare(b.short_email)
  })

  const handleRefresh = () => {
    fetchUsers(accessToken).then(() => {
      console.debug("Usuarios refrescados")
    })
  }

  useEffect(() => {
    (userList.length === 0 && accessToken) && fetchUsers(accessToken).then(() => {
      console.debug("Se obtuvieron los usuarios: ", userList)
    })
  }, [userList, accessToken, fetchUsers])

  return (
    <div className='policies'>
      <CreateUser></CreateUser>
      <h1>Lista de Usuarios</h1>
      <div className='container-row align-end'>
        <div className='search-policies'>
          <div className='input-form'>
            <label>Short Email del Usuario</label>
            <TextField type='text' onChange={(e) => { setEmail(e.target.value) }}></TextField>
          </div>
        </div>
        <br></br>
        <Button style={{
          margin: "16px"
        }} startIcon={<Refresh />} variant='contained' onClick={handleRefresh}>Refrescar</Button>
      </div>

      <Table className='table'>
        <TableHead>
          <TableRow>
            <TableCell component={"th"}>ID</TableCell>
            <TableCell component={"th"}>Email</TableCell>
            <TableCell component={"th"}>Short Email</TableCell>
            <TableCell component={"th"}>SAP ID</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {filterUsers.length > 0 && filterUsers.map((user: UserListInterface) => {
            return <TableRow>
              <TableCell>{user.id}</TableCell>
              <TableCell>{user.large_email}</TableCell>
              <TableCell>{user.short_email}</TableCell>
              <TableCell>{user.sap_id}</TableCell>
            </TableRow>
          })}
        </TableBody>
      </Table>
      <p></p>
    </div>
  )
}

export default Users