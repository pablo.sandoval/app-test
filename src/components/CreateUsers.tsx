import { Button } from '@mui/material'
import React, { Fragment, useState } from 'react'
import * as XLSX from 'xlsx'
import { useAuth, useUser } from '../lib'
import { useTestHook } from '../hooks/useTestHook'

const CreateUsers = () => {

  const { createUser } = useTestHook()
  const { accessToken } = useAuth()

  const [excelFile, setExcelFile] = useState<any>()

  const [excelData, setExcelData] = useState<any>()

  const handleFile = (e: any) => {
    const selectedFile = e.target.files[0];
    if (selectedFile) {
      console.log(selectedFile)
      const readerFile = new FileReader()
      readerFile.readAsArrayBuffer(selectedFile)
      readerFile.onload = (e) => {
        setExcelFile(e.target?.result)
      }
    } else {
      console.log('Selecciona un archivo porfavor.')
    }
    return e
  }
  const handleUpload = (e: any) => {
    if (excelFile) {
      const workbook = XLSX.read(excelFile, { type: 'buffer', })
      const workname = workbook.SheetNames[0]
      const worksheet = workbook.Sheets[workname]
      const data = XLSX.utils.sheet_to_json(worksheet)
      console.debug(data)
      setExcelData(data)
    }
  }
  const handleCreateUsers = () => {
    if (excelData) {
      const promises: Array<any> = []
      excelData.forEach((user: any, index: number) => {
        const newUser = user;
        newUser.sap_id = null
        promises.push(new Promise(() => setTimeout(() => {
          createUser(newUser, accessToken).then(() => {
            console.log(`${newUser.short_email} creado con éxito.`)
          })
          return true
        }, 2000 + 2000 * index)))
      });
      Promise.all(promises).then((values) => {
        console.log('Todos los usuarios creado con éxito.',values)
      })
      console.debug('Cargando...')
      return 
    }
    console.debug('No hay datos cargados')
  }

  return (<Fragment>
    <div style={{
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      flexDirection: "column",
    }}>
      <input style={{
        marginBottom: '12px'
      }} type='file' accept='.xlsx' onChange={(e) => {
        console.debug(e)
        handleFile(e)
      }}></input>
      <div className='container-row'>
        <Button variant='outlined' onClick={handleUpload}>Cargar Datos</Button>
        <Button variant='contained' onClick={handleCreateUsers}>Crear Nuevos Usuarios</Button>
      </div>

    </div>

  </Fragment>

  )
}

export default CreateUsers