import React, { useState } from 'react'
import { useTestHook } from '../hooks/useTestHook'
import { useAuth } from '../lib'
import { Button, TextField } from '@mui/material'
import { Add } from '@mui/icons-material'

const CreateUser = () => {

  const { createUser } = useTestHook()
  const { accessToken } = useAuth()
  const [message, setMessage] = useState<string>()
  const [form, setForm] = useState({
    short_email: "",
    large_email: "",
    sap_id: null
  })

  const handleClick = () => {
    const customMessage = []
    if (form.short_email === "") {
      customMessage.push("Falta escribir un short email.")
    }
    if (form.large_email === "") {
      customMessage.push("Falta escribir un large email.")
    }
    if (customMessage.length <= 0) {
      createUser(form, accessToken).then(() => {
        setMessage("Usuario creado con éxito.")
      })
    } else {
      setMessage(customMessage.join("/"))
    }
  }
  return (
    <div style={{
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      flexDirection: "column"
    }}>
      <h1>Crear Usuario</h1>
      <div className='container-row'>
        <div className='input-form'>
          <label>Short Email</label>
          <TextField type='text' onChange={(e) => {
            setForm({ ...form, short_email: e.target.value })
          }}></TextField>
        </div>
        <div className='input-form'>
          <label>Large Email</label>
          <TextField type='text' onChange={(e) => {
            setForm({ ...form, large_email: e.target.value })
          }}></TextField>
        </div>
      </div>

      <Button style={{
          margin: "16px"
        }} endIcon={<Add />} variant='contained' onClick={handleClick}>Crear Nuevo Usuario</Button>
      <h4>{message}</h4>
    </div>
  )
}

export default CreateUser