import React, { useState } from 'react'
import { useAuth } from '../lib'
import jwtDecode from 'jwt-decode'
import { Button,Table, TableBody,TableCell,TableHead,TableRow } from '@mui/material'
import '../App.css'

const Authentication = () => {
  const { isAuthenticated, logIn, logOut, user, accessToken} = useAuth()
  const [showToken, setShowToken] = useState(false)

  //console.debug(accessToken ? jwtDecode(accessToken) : "No existe token")
  return (
    <>
      <p>{isAuthenticated ? "Autenticado" : "No Autenticado"}</p>
      <p>{user && user.email}</p>
      {accessToken && <Button variant='contained' onClick={() => {
        setShowToken(!showToken)
      }}>{`${showToken ? "Ocultar" : "Mostrar"}`} Token</Button>}
      <p>{(accessToken && showToken) && accessToken}</p>
      <div>
        <Button variant='contained' onClick={() => { logIn(import.meta.env.VITE_APP) }}>Login</Button>
        <Button variant='contained' onClick={logOut}>LogOut</Button>
      </div>
      {user && user.tenants && user.tenants.map((tenant) => {
        return <div className='tenant'>
          <h1>{tenant.name}</h1>
          <p>Tipo: {tenant.type}</p>
          <Table className='table'>
            <TableHead>
            <TableRow>
              <TableCell>Nombre</TableCell>
              <TableCell>Descripción</TableCell>
              <TableCell>URL</TableCell>
              <TableCell>Permisos</TableCell>
            </TableRow>
            </TableHead>
            <TableBody>
            {tenant.applications.map((app) => {
              return <TableRow>
                <TableCell>{app.name}</TableCell>
                <TableCell>{app.description}</TableCell>
                <TableCell>{app.url}</TableCell>
                <TableCell className='permission-list'>{app.permissions && app.permissions.map((permission) => {
                  return <div>{permission}</div>
                })}</TableCell>
              </TableRow>
            })}
            </TableBody>
            
          </Table>
        </div>
      })}
    </>
  )
}

export default React.memo(Authentication)