import React, { useEffect, useState } from 'react'
import { useTestHook } from '../hooks/useTestHook'
import { useAuth, usePermission, usePolicies } from '../lib'
import { Button, Select, MenuItem, Table, TableBody,TableHead,TableRow, TableCell } from '@mui/material'
import { Add, Delete } from '@mui/icons-material'

interface PermissionInterface {
  id: number,
  name: string,
  description: string,
  status: boolean
}

interface FormCreatePolicyInterface {
  user_id: number,
  application_id: number,
  tenant_id: number,
  role_id: number,
  permissions: number[]
}

interface AppInterface {
  description: string,
  id: number,
  name: string,
  url: string
}

interface UserInterface {
  id: number,
  short_email: string,
  large_email: string,
  sap_id: string
}

interface TenantInterface {
  id: number,
  name: string,
  type: string,
  status: boolean
}

const CreatePolicy = () => {
  const [listPermissions, setListPermissions] = useState<PermissionInterface[]>([])
  const [listUsers, setListUsers] = useState<UserInterface[]>([])
  const [listApps, setListApps] = useState<AppInterface[]>([])
  const [listTenants, setListTenants] = useState<TenantInterface[]>([])
  const [selectedPermissions, setSelectedPermissions] = useState<PermissionInterface[]>([])
  const [loading, setLoading] = useState<boolean>(false)
  const [message, setMessage] = useState<string>("")
  const [form, setForm] = useState<FormCreatePolicyInterface>({
    application_id: 0,
    permissions: [],
    role_id: 0,
    tenant_id: 0,
    user_id:0
  })
  const { getPermissions, permissions, getUsers, users, getApps, apps, getTenants, } = useTestHook()
  const { createPolicy } = usePolicies()
  const { accessToken } = useAuth()
  const { fetchPermissionsByRole,permissionsByRole, isLoadingPermissions } = usePermission()
  console.debug("Lista de permisos: ",listPermissions)
  console.debug(listPermissions.length > 0 && listPermissions[0].name.split(".")[0])
  const handleCreatePolicy = () => {
    const sendForm = { ...form, permissions: selectedPermissions.map((permission) => permission.id) }
    console.debug(sendForm)
    if (form) {
      const customMessage = []

      if (sendForm.application_id == 0) {
        customMessage.push("Falta escribir aplicacion ")
      }
      if (sendForm.user_id == 0) {
        customMessage.push("Falta escribir usuario ")
      }
      if (sendForm.role_id == 0) {
        customMessage.push("Falta escribir rol ")
      }
      if (sendForm.tenant_id == 0) {
        customMessage.push("Falta escribir tenant ")
      }
      if (sendForm.permissions.length <= 0) {
        customMessage.push("No hay permisos seleccionados")
      }
      if (customMessage.length > 0) {
        setMessage(customMessage.join(" / "))
        setLoading(false)
        return
      }
      console.debug("Se envía el formulario ", sendForm)
      createPolicy(accessToken, sendForm).then(() => {
        setMessage("Política creada con éxito.")
        setLoading(false)
      }).catch((e:any) => {
        setMessage("Ocurrio un error al crear la política." + JSON.stringify(e))
        setLoading(false)
      })
      return
    }
    setMessage("No se ha completado el formulario")
    setLoading(false)
  }

  const handleChangeRole = () => {
    if(form.role_id !== 0){
      fetchPermissionsByRole(accessToken,form.role_id).then((response:any) => {
        console.debug("Hola")
        const sortPermissions = response.sort((a: PermissionInterface, b: PermissionInterface) => {
          return a.name.localeCompare(b.name)
        })
        setListPermissions(sortPermissions)
      })
    }
  }
  const filterList = listPermissions.filter((permission) => !selectedPermissions.find((selectedPermission) => permission.id === selectedPermission.id) && listApps.find((app) => app.id === form.application_id)?.name ===  permission.name.split(".")[0])
  useEffect(() => {
    if (accessToken) {
      getUsers(accessToken).then((e) => {
        const orderUsers = e.sort((a: UserInterface, b: UserInterface) => {
          return a.short_email.localeCompare(b.short_email)
        })
        setListUsers(orderUsers)
      })
      getApps(accessToken).then((e) => {
        const orderApps = e.sort((a: PermissionInterface, b: PermissionInterface) => {
          return a.name.localeCompare(b.name)
        })
        setListApps(orderApps)
      })
      getTenants(accessToken).then((e) => {
        const orderTenants = e.sort((a: TenantInterface, b: TenantInterface) => {
          return a.name.localeCompare(b.name)
        })
        setListTenants(orderTenants)
      })
    }
  }, [])
  return (
    <div className='form-policy'>
      <h1>Crear Política</h1>
      <div className='container-row'>
        <div className='input-form'>
          <label>Email Usuario</label>
          <Select style={{
            width: 328
          }} onChange={(e) => {
            console.debug(e.target.value)
            if (Number(e.target.value) !== 0) {
              setForm({ ...form, user_id: Number(e.target.value) })
            }
          }}>
            {!form?.user_id && <MenuItem value={0}>{">>"}Elige un usuario{"<<"}</MenuItem>}
            {listUsers
              .map((user, index: number) => {
                return <MenuItem value={user.id}>{user.short_email}</MenuItem>
              })}
          </Select>
        </div>
        <div className='input-form'>
          <label>Planta</label>
          <Select style={{
            width: 328
          }} onChange={(e) => {
            if (Number(e.target.value) !== 0) setForm({ ...form, tenant_id: Number(e.target.value) })
          }}>
            {!form?.tenant_id && <MenuItem value={0}>{">>"}Elige un tenant{"<<"}</MenuItem>}
            {listTenants
              .map((tenant, index: number) => {
                return <MenuItem value={tenant.id}>{tenant.name}</MenuItem>
              })}
          </Select>
        </div>
      </div>
      <div className='container-row'>
        <div className='input-form'>
          <label>Aplicación</label>
          <Select style={{
            width: 328
          }} onChange={(e) => {
            if (Number(e.target.value) !== 0) setForm({ ...form, application_id: Number(e.target.value) })
          }}>
            {!form?.application_id && <MenuItem value={0}>{">>"}Elige un usuario{"<<"}</MenuItem>}
            {listApps
              .map((app, index: number) => {
                return <MenuItem value={app.id}>{app.name}</MenuItem>
              })}
          </Select>
        </div>
        <div className='input-form'>
          <label>Rol Principal</label>
          <Select style={{
            width: 328
          }} onChange={(e) => {
            if (Number(e.target.value) !== 0) {
              setForm({ ...form, role_id: Number(e.target.value) })
              handleChangeRole()
            } 
          }}>
            {!form?.role_id && <MenuItem value={0}>{">>"}Elige un Rol{"<<"}</MenuItem>}
            <MenuItem value={1}>Operador</MenuItem>
            <MenuItem value={2}>Administrador</MenuItem>
            <MenuItem value={3}>Gestor</MenuItem>
          </Select>
        </div>
      </div>
      <div className='container-row'>
        <div className='input-form'>
          <label>Permisos</label>
          {(form.role_id === 0 || form.application_id === 0 ) ? <Select style={{
            width: 328
          }} disabled></Select>
          :<Select style={{
            width: 328
          }} onChange={(e) => {
            if (Number(e.target.value) !== 0) setSelectedPermissions([...selectedPermissions, filterList[Number(e.target.value) - 1]])
          }}>
            {selectedPermissions.length <= 0 && <MenuItem value={0}>{">>"}Elige un permiso{"<<"}</MenuItem>}
            {listPermissions.filter((permission) => !selectedPermissions.find((selectedPermission) => permission.id === selectedPermission.id) && listApps.find((app) => app.id === form.application_id)?.name ===  permission.name.split(".")[0])
              .map((permission: PermissionInterface, index: number) => {
                return <MenuItem value={index+1}>{permission.name}</MenuItem>
              })}
          </Select>}
          
        </div>
      </div>
      <div>
        {!loading
          ? <Button style={{
            margin: "16px"
          }} variant='contained' endIcon={<Add />} onClick={() => {
            setLoading(true)
            setMessage("Cargando...")
            handleCreatePolicy()
          }}>Crear Nueva Política</Button>
          : <Button variant='contained' endIcon={<Add />} disabled>Crear Nueva Política</Button>}
        <h5>{message}</h5>
      </div>
      <h2>Permisos Seleccionados</h2>
      <Table style={{
        width: 900
      }}>
        <TableHead>
        <TableRow>
          <TableCell component='th'>ID</TableCell>
          <TableCell component='th'>Nombre</TableCell>
          <TableCell component='th'>Descripcion</TableCell>
          <TableCell component='th'>Acción</TableCell>
        </TableRow>
        </TableHead>
        <TableBody>
        {selectedPermissions && selectedPermissions.map((permission, index) => {
          return <TableRow>
            <TableCell>{permission.id}</TableCell>
            <TableCell>{permission.name}</TableCell>
            <TableCell>{permission.description}</TableCell>
            <TableCell align='center'><Delete style={{
              color: "red",
              cursor: "pointer"
            }} onClick={() => {
              const newSelectedPermissions = selectedPermissions
              console.debug("index: ", index)
              newSelectedPermissions.splice(index, 1)
              console.debug(newSelectedPermissions)
              setSelectedPermissions([...newSelectedPermissions])
            }}>X</Delete></TableCell>
          </TableRow>
        })}
        </TableBody>
      </Table>
    </div>
  )
}

export default CreatePolicy