
import React from 'react'
import { HookContextInterface } from "../Context/HookContextInterface";

export const useHookManager = () : HookContextInterface => {

  const [apps,setApps] = React.useState([])
  const [policys,setPolicys] = React.useState([])
  const [users,setUsers] = React.useState([])
  const [permissions,setPermissions] = React.useState([])
  const [tenants,setTenants] = React.useState([])
  const [roles,setRoles] = React.useState([])
  const [roleSelected,setRoleSelected] = React.useState(null)

  //Aplicaciones

  const createApp = () => {
    return Promise.all([() => { return true }])
  }
  const getApps = async (accessToken?:string) => {
    console.debug("Obteniendo Aplicaciones")
    const url =
      import.meta.env.VITE_API + "/api/v1/applications";
    try {
      const response = await fetch(url, {
        method: "GET",
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      }).then((response) => {
        const decodeResponse = response.json();
        return decodeResponse;
      });
      if (response.message) {
        console.log(`Error ocurrido con: ${url}`);
        console.log(response)
        return Promise.reject(response.message);
      }
      const result = await response;
      setApps(result)
      console.log(result);
      return Promise.resolve(result);
    } catch (error) {
      console.log(`Error ocurrido con ${url}`);
      console.log(error);
      return Promise.reject(null);
    }
  }

  //Políticas

  const createPolicy = async (accessToken?:string,form?: any) => {
    console.debug("Creando Política de un Usuario", form)
    const sendForm = JSON.stringify({
      "application_id": form.application_id,
      "tenant_id": form.tenant_id,
      "role_id": form.role_id,
      "permissions": form.permissions
    })
    console.debug("sendingForm: ",sendForm)
    const url =
      import.meta.env.VITE_API + "/api/v1/users/"+ form.user_id + "/policy";
    try {
      const response = await fetch(url, {
        method: "POST",
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${accessToken}`,
        },
        body: (sendForm)
      }).then((response) => {
        const decodeResponse = response.json();
        return decodeResponse;
      });
      if (response.message) {
        console.log(`Error ocurrido con: ${url}`);
        console.log(response)
        return Promise.reject(response.message);
      }
      const result = await response;
      console.log(result);
      return Promise.resolve(result);
    } catch (error) {
      console.log(`Error ocurrido con ${url}`);
      console.log(error);
      return Promise.reject(null);
    }
  }
  const deletePolicy = async (accessToken?:string,id?:number) => {
    console.debug("Borrando Policy")
    const url =
      import.meta.env.VITE_API + "/api/v1/policies/" + id;
    console.debug(url)
    try {
      const response = await fetch(url, {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      }).then((response) => {
        const decodeResponse = response.json();
        return decodeResponse;
      });
      if (response.message) {
        console.log(`Error ocurrido con: ${url}`);
        console.log(response)
        return Promise.reject(response.message);
      }
      const result = await response;
      console.log(result);
      return Promise.resolve(result);
    } catch (error) {
      console.log(`Error ocurrido con ${url}`);
      console.log(error);
      return Promise.reject(null);
    }
  }
  const getPolicies = async (accessToken?:string) => {
    console.debug("Obteniendo Policies")
    const url =
      import.meta.env.VITE_API + "/api/v1/policies";
    console.debug(url)
    try {
      const response = await fetch(url, {
        method: "GET",
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      }).then((response) => {
        const decodeResponse = response.json();
        return decodeResponse;
      });
      if (response.message) {
        console.log(`Error ocurrido con: ${url}`);
        console.log(response)
        return Promise.reject(response.message);
      }
      const result = await response;
      setPolicys(result)
      console.log(result);
      return Promise.resolve(result);
    } catch (error) {
      console.log(`Error ocurrido con ${url}`);
      console.log(error);
      return Promise.reject(null);
    }
  }

  //Usuarios

  const createUser = async (form: any,accessToken?:string) => {
    console.debug("Creando nuevo Usuario")
    const url =
      import.meta.env.VITE_API + "/api/v1/users/";
    try {
      const response = await fetch(url, {
        method: "POST",
        headers: {
          Authorization: `Bearer ${accessToken}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          short_email: form.short_email,
          large_email: form.large_email,
          sap_id: form.sap_id,
        })
      }).then((response) => {
        const decodeResponse = response.json();
        return decodeResponse;
      });
      if (response.message) {
        console.log(`Error ocurrido con: ${url}`);
        console.log(response)
        return Promise.reject(response.message);
      }
      const result = await response;
      console.log(result);
      return Promise.resolve(result);
    } catch (error) {
      console.log(`Error ocurrido con ${url}`);
      console.log(error);
      return Promise.reject(null);
    }
  }
  const getUsers = async (accessToken?:string) => {
    console.debug("Obteniendo Aplicaciones")
    const url =
      import.meta.env.VITE_API + "/api/v1/users";
    try {
      const response = await fetch(url, {
        method: "GET",
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      }).then((response) => {
        const decodeResponse = response.json();
        return decodeResponse;
      });
      if (response.message) {
        console.log(`Error ocurrido con: ${url}`);
        console.log(response)
        return Promise.reject(response.message);
      }
      const result = await response;
      setUsers(result)
      console.log(result);
      return Promise.resolve(result);
    } catch (error) {
      console.log(`Error ocurrido con ${url}`);
      console.log(error);
      return Promise.reject(null);
    }
  }

  //Permissions
  const getPermissions = async (accessToken?:string) => {
    console.debug("Obteniendo Aplicaciones")
    const url =
    import.meta.env.VITE_API + "/api/v1/permissions";
    try {
      const response = await fetch(url, {
        method: "GET",
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      }).then((response) => {
        const decodeResponse = response.json();
        return decodeResponse;
      });
      if (response.message) {
        console.log(`Error ocurrido con: ${url}`);
        console.log(response)
        return Promise.reject(response.message);
      }
      const result = await response;
      setPermissions(result)
      console.log(result);
      return Promise.resolve(result);
    } catch (error) {
      console.log(`Error ocurrido con ${url}`);
      console.log(error);
      return Promise.reject(null);
    }
  }

  //Roles
  const getRoles = async (accessToken?:string) => {
    console.debug("Obteniendo Roles")
    const url =
    import.meta.env.VITE_API + "/api/v1/roles";
    try {
      const response = await fetch(url, {
        method: "GET",
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      }).then((response) => {
        const decodeResponse = response.json();
        return decodeResponse;
      });
      if (response.message) {
        console.log(`Error ocurrido con: ${url}`);
        console.log(response)
        return Promise.reject(response.message);
      }
      const result = await response;
      setRoles(result)
      console.log(result);
      return Promise.resolve(result);
    } catch (error) {
      console.log(`Error ocurrido con ${url}`);
      console.log(error);
      return Promise.reject(null);
    }
  }
  const getRolePermissions = async (accessToken?:string,id?:number) => {
    console.debug("Obteniendo Permisos del rol ",id)
    const url =
    import.meta.env.VITE_API + "/api/v1/roles/" + id + "/permissions";
    try {
      const response = await fetch(url, {
        method: "GET",
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      }).then((response) => {
        const decodeResponse = response.json();
        return decodeResponse;
      });
      if (response.message) {
        console.log(`Error ocurrido con: ${url}`);
        console.log(response)
        return Promise.reject(response.message);
      }
      const result = await response;
      setRoleSelected(result)
      console.log(result);
      return Promise.resolve(result);
    } catch (error) {
      console.log(`Error ocurrido con ${url}`);
      console.log(error);
      return Promise.reject(null);
    }
  }
  //Tenants

  const getTenants = async (accessToken?:string) => {
    console.debug("Obteniendo Tenants")
    const url =
    import.meta.env.VITE_API + "/api/v1/tenants";
    try {
      const response = await fetch(url, {
        method: "GET",
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      }).then((response) => {
        const decodeResponse = response.json();
        return decodeResponse;
      });
      if (response.message) {
        console.log(`Error ocurrido con: ${url}`);
        console.log(response)
        return Promise.reject(response.message);
      }
      const result = await response;
      setTenants(result)
      console.log(result);
      return Promise.resolve(result);
    } catch (error) {
      console.log(`Error ocurrido con ${url}`);
      console.log(error);
      return Promise.reject(null);
    }
  }

  return {
    createApp,
    createPolicy,
    createUser,
    deletePolicy,
    getApps,
    getPolicies,
    getUsers,
    getPermissions,
    getTenants,
    getRoles,
    getRolePermissions,
    apps,
    tenants,
    policys,
    users,
    permissions,
    roles,
    roleSelected
  }
}