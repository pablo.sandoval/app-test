import {useContext} from 'react'

import { HookContextInterface } from "./Context/HookContextInterface";
import { HookContext } from './Context/HookContext';

export const useTestHook = (): HookContextInterface => useContext(HookContext)