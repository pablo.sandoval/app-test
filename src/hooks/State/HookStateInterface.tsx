export interface HookStateInterface {
  policys: any[],
  users: any[],
  apps: any[],
  permissions: any[],
  tenants: any[],
  roles: any[],
  roleSelected: any
}