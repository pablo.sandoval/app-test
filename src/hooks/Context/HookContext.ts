import { createContext } from 'react'
import { HookContextInitialState } from './HookContextInitialState'
import { HookContextInterface } from './HookContextInterface'

export const HookContext = createContext<HookContextInterface>(
  HookContextInitialState
)