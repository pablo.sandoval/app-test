import { HookStateInterface } from "../State/HookStateInterface"

export interface HookContextInterface extends HookStateInterface {
  getPolicies: (accessToken?:string) => Promise<any>
  getUsers: (accessToken?:string) => Promise<any>
  getApps: (accessToken?:string) => Promise<any>
  getPermissions: (accessToken?:string) => Promise<any>
  getTenants: (accessToken?:string) => Promise<any>
  getRoles: (accessToken?:string) => Promise<any>
  getRolePermissions: (accessToken?:string) => Promise<any>
  createPolicy: (accessToken?:string,form?:any) => Promise<any>
  deletePolicy: (accessToken?:string,id?:number) => Promise<any>
  createApp: (accessToken?:string) => Promise<any>
  createUser: (form:any,accessToken?:string) => Promise<any>
}