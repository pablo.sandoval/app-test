import { HookContextInterface } from "./HookContextInterface";
import { HookInitialState } from "../State/HookInitialState";

const stub = () => {
  throw new Error(
    "Debe agregar provider -> <AuthProvider>{component}</AuthProvider"
  );
};

export const HookContextInitialState: HookContextInterface = {
  createApp: stub,
  createPolicy: stub,
  createUser: stub,
  deletePolicy: stub,
  getApps: stub,
  getPolicies: stub,
  getUsers: stub,
  getPermissions: stub,
  getTenants: stub,
  ...HookInitialState
}