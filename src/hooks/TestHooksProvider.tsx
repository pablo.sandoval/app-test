import React from 'react'
import { HookContextInterface } from './Context/HookContextInterface'
import { useHookManager } from './Manager/HookManager'
import { HookContext } from './Context/HookContext';

interface TestHooksProviderInterface {
  children: JSX.Element | JSX.Element[];
}

const TestHooksProvider = ({
  children
}: TestHooksProviderInterface) => {
  const {
    apps,
    createApp,
    createPolicy,
    createUser,
    deletePolicy,
    getApps,
    getPolicies,
    getUsers,
    getPermissions,
    getTenants,
    getRolePermissions,
    getRoles,
    permissions,
    tenants,
    policys,
    users,
    roleSelected,
    roles
  }: HookContextInterface = useHookManager();

  const providerHook = {
    apps,
    createApp,
    createPolicy,
    createUser,
    deletePolicy,
    getApps,
    getPolicies,
    getUsers,
    getPermissions,
    getTenants,
    getRolePermissions,
    getRoles,
    permissions,
    tenants,
    policys,
    users,
    roleSelected,
    roles
  }
  return (
    <HookContext.Provider value={providerHook}>
      {children}
    </HookContext.Provider>
  )
}

export default TestHooksProvider