import { createContext } from "react"
import { FormStateInitialState, FormStateInterface } from "./FormInitialState"

export interface FormContextInterface {
  setForm: React.Dispatch<FormStateInterface>
  form: FormStateInterface
}

export const FormContextInitialState = {
  setForm: () => false,
  form: FormStateInitialState
}

export const FormContext = createContext<FormContextInterface>(FormContextInitialState)