//General
import React, { useState, memo, useContext, useMemo } from 'react'

//Context
import { FormContext } from './FormContext'
import { FormStateInterface } from './FormInitialState'

interface FormProviderInterface {
  children: JSX.Element | JSX.Element[]
}

const FormProvider = ({
  children
}: FormProviderInterface) => {
  const context = useContext(FormContext)
  const [form, setForm] = useState<FormStateInterface>(context.form)

  const providerValues = useMemo(() => {
    return { setForm, form }
  }, [setForm, form])

  return (
    <FormContext.Provider value={providerValues}>{children}</FormContext.Provider>
  )
}

export default memo(FormProvider)