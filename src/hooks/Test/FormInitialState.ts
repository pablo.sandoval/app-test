export interface FormStateInterface {
  inputA: string,
  inputB: string,
  inputC?: string
}

export const FormStateInitialState = {
  inputA: "",
  inputB: "",
  inputC: undefined
}