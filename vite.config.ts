import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import tsconfigPaths from 'vite-tsconfig-paths';
import svgrPlugin from 'vite-plugin-svgr';
import commonjs from 'vite-plugin-commonjs';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react(), tsconfigPaths(), svgrPlugin(),commonjs()],
  server:{
    port: 3000
  },
  esbuild: {
    logOverride: { 'this-is-undefined-in-esm': 'silent' },
    },
  
});